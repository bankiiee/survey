//
//  CustomCollectionViewCell.h
//  Survey
//
//  Created by Sakarn Limnitikarn on 8/27/2558 BE.
//  Copyright (c) 2558 Sakarn Limnitikarn. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol CustomCollectionViewCellDelegate <NSObject>
-(void) takeSurveyButtonForItem:(NSDictionary*) itemInfo;
@end
@interface CustomCollectionViewCell : UICollectionViewCell
@property (nonatomic, strong) NSDictionary *itemInfo;
@property (nonatomic, strong) id<CustomCollectionViewCellDelegate> delegate;
@end
