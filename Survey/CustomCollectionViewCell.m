//
//  CustomCollectionViewCell.m
//  Survey
//
//  Created by Sakarn Limnitikarn on 8/27/2558 BE.
//  Copyright (c) 2558 Sakarn Limnitikarn. All rights reserved.
//

#import "CustomCollectionViewCell.h"
#define kTagForBackgroundImage 10
#define kTagForItemTitle 11
#define kTagForItemSubTitle 12
#define kTagForItemButton 13
@implementation CustomCollectionViewCell

-(void) awakeFromNib{
    //TODO: set round corner to button
    UIButton *button = (UIButton*)[self viewWithTag:kTagForItemButton];
    [button.layer setCornerRadius:button.frame.size.height/2];
    [button addTarget:self action:@selector(takeSurveyButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
}
-(void) takeSurveyButtonPressed: (id) sender{
    //TODO: protocol method
    if (self.delegate!= nil && [self.delegate respondsToSelector:@selector(takeSurveyButtonForItem:)]){
        [self.delegate takeSurveyButtonForItem:self.itemInfo];
    }
}
@end
