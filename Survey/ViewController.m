//
//  ViewController.m
//  Survey
//
//  Created by Sakarn Limnitikarn on 8/26/15.
//  Copyright (c) 2015 Sakarn Limnitikarn. All rights reserved.
//

#import "ViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <AFNetworking/AFNetworking.h>
#import <KVNProgress/KVNProgress.h>
#import "CustomCollectionViewCell.h"
#import "DetailViewController.h"
#define kCallingURL @"https://www-staging.usay.co/app/surveys.json"
#define kCallingURLCredential @"6eebeac3dd1dc9c97a06985b6480471211a777b39aa4d0e03747ce6acc4a3369"
#define kTagForBackgroundImage 10
#define kTagForItemTitle 11
#define kTagForItemSubTitle 12
#define kTagForItemButton 13
@interface ViewController () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, CustomCollectionViewCellDelegate>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property CGRect oldStateOfPageControl;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *reloadButton;
@property (nonatomic, strong) NSMutableArray *listOfItem;
- (IBAction)reloadButtonPressed:(id)sender;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self prepareForDataSource];
    
}
-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
}
-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.oldStateOfPageControl = self.pageControl.frame;
    [self adjustPageControl];
    
}
-(void) viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator{
    //TODO: handle device re-orientation
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        [self.collectionView.collectionViewLayout invalidateLayout];
        [self.collectionView reloadData];
        [self.collectionView setContentOffset:CGPointMake(0, 0) animated:YES];
        
    } completion:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        [self adjustPageControl];
    }];
    
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
}


#pragma mark - Adjust Position for Page Control
-(void) adjustPageControl{
    //TODO: rotate the page control to vertical style
    [self.pageControl setFrame:self.oldStateOfPageControl];
    [self.pageControl setFrame:CGRectMake(self.pageControl.frame.origin.x, self.pageControl.frame.origin.y, self.view.frame.size.height - 100, self.pageControl.frame.size.height)];
    [self.pageControl setCenter:CGPointMake(self.view.frame.size.width - self.pageControl.frame.size.height, self.view.frame.size.height / 2)];
    [self.pageControl setTransform:CGAffineTransformMakeRotation(M_PI_2)];
    
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    [self.pageControl setNumberOfPages:self.listOfItem.count];
    return self.listOfItem.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CustomCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    NSDictionary *itemInfo = self.listOfItem[indexPath.row];
    
    UILabel *title = (UILabel*)[cell viewWithTag:kTagForItemTitle];
    [title setText:itemInfo[@"title"]];
    
    UILabel *subTitle = (UILabel*)[cell viewWithTag:kTagForItemSubTitle];
    [subTitle setText:itemInfo[@"description"]];
    
    UIImageView *imageView = (UIImageView*)[cell viewWithTag:kTagForBackgroundImage];
    [imageView setAlpha:0.5];
    if (itemInfo[@"cover_image_url"] != nil && ![itemInfo[@"cover_image_url"] isEqualToString:@""]) {
        //TODO: use SDWebImage to load image from URL asynchronously
        [imageView sd_setImageWithURL:[NSURL URLWithString:itemInfo[@"cover_image_url"]] placeholderImage:[UIImage imageNamed:@"placeholder-image"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                [imageView setAlpha:0];
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                    [imageView setImage:image];
                    [imageView setAlpha:0.5];
                } completion:^(BOOL finished) {
                    
                }];
            }];
            [imageView setImage:image];
        }];
    }else{
        [imageView setImage:[UIImage imageNamed:@"placeholder-image"]];
    }
    
    [cell setItemInfo:itemInfo];
    [cell setDelegate:self];
    [cell layoutIfNeeded];
    return cell;
}
#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"Current IndexPath: %@", indexPath);
    [self.pageControl setCurrentPage:indexPath.row];
}
#pragma mark - UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    [collectionView layoutIfNeeded];
    [self.collectionView layoutIfNeeded];
    return CGSizeMake(self.collectionView.frame.size.width, self.collectionView.frame.size.height);
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsZero;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}

#pragma mark - Datasource Setup
-(void) prepareForDataSource{
    [self showHUD];
    //TODO: use AFNetworking to load async json data from remote
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:@"usay" password:@"isc00l"];
    NSString *url = [NSString stringWithFormat:@"%@?access_token=%@", kCallingURL, kCallingURLCredential];
    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        self.listOfItem = [NSMutableArray arrayWithArray:responseObject];
        [self.collectionView reloadData];
        [self.collectionView setContentOffset:CGPointMake(0, 0) animated:YES];
        [self hideHUD];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [KVNProgress showErrorWithStatus:@"Unable to load the request, please try again later." completion:^{
            [self.reloadButton setEnabled:YES];
            
        }];
    }];
}
#pragma mark - MBProgressHUD
-(void) showHUD{
    //TODO: show HUD while loading json and disable reload button
    [KVNProgress showWithStatus:@"Loading"];
    [self.reloadButton setEnabled:NO];
    
}
-(void) hideHUD{
    //TODO: hide hud and enable reload button
    [KVNProgress dismiss];
    [self.reloadButton setEnabled:YES];
}
- (IBAction)reloadButtonPressed:(id)sender {
    //TODO: reload all from remote
    [self prepareForDataSource];
}
#pragma mark - CustomCollectionViewCellDelegate
-(void) takeSurveyButtonForItem:(NSDictionary*) itemInfo{
    NSLog(@"itemInfo: %@", itemInfo);
    [self performSegueWithIdentifier:@"showItemDetail" sender:itemInfo];
}

#pragma mark - Navigation
-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"showItemDetail"]) {
        //prepare iteminfo
    }
}
@end
